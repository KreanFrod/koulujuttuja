package com.example.hello;

        import org.springframework.web.bind.annotation.RestController;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.PathVariable;


@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String hello() {
        return "Hyvää päivää!";
    }

    @RequestMapping("/{kayttajanimi}/bye")
    public String bye(@PathVariable("kayttajanimi") String kayttajanimi){
            return "Love you " + kayttajanimi;
        }
    }